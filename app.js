import { Vehicle } from "./vehicle.js";
import { Car } from "./car.js";
import { Motorbike } from "./motobike.js";
var ve1 = new Vehicle("Toyota", 2022);
var ve2 = new Vehicle("BMW", 2023);
console.log(ve1.print());
console.log(ve2.print());


var car1 = new Car("IR-1193", "Track");
var car2 = new Car("BMW-4493", "White");
console.log(car1.honk());
console.log(car2.honk());


var moto1 = new Motorbike("Yamaha", "Track");
var moto2 = new Motorbike("Toyota", "White");
console.log(moto1.honk());
console.log(moto2.honk());
