import { Vehicle } from "./vehicle.js";

class Car extends Vehicle {
    vId;
    modelName;

    constructor (paramId, paramModelName){
        super(paramId, paramModelName);
        this.vId = paramId;
        this.modelName = paramModelName;
    }
     
    honk(){
        console.log(this.vId);
        console.log(this.modelName);
    }
}

export {
    Car
}