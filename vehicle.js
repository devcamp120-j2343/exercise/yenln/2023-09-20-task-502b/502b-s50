
class Vehicle {
    _brand;
    _yearManufactured;

    constructor(paramBrand, paramYear) {
        this._brand = paramBrand;
        this._yearManufactured = paramYear;
    }
    print() {
        console.log(this._brand);
        console.log(this._yearManufactured);
    }
}

export{
    Vehicle
}